package org.matheclipse;/**
 * Created by Пользователь on 02.05.2016.
 */

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import org.matheclipse.controllers.OverviewController;
import org.matheclipse.core.expression.F;
import org.matheclipse.core.interfaces.IExpr;
import org.matheclipse.script.engine.SymjaScriptEngine;

import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;


public class Main extends Application {

    final ScriptEngineManager scriptManager;

    final ScriptEngine consoleEngine;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Ученический модуль");
        // Загрузка сцены
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Main.class.getResource("OverviewStart.fxml"));
        VBox overview = null;
        try {
            overview = (VBox) loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        primaryStage.setResizable(false);

        Scene scene = new Scene(overview);
        primaryStage.setScene(scene);
        primaryStage.getIcons().add(new Image(Main.class.getResourceAsStream("icon.png")));
        OverviewController controller = loader.getController();
        controller.setMain(this);


        primaryStage.show();


}
    // собственно функция, вычисляющая ответ (SymJa)
    public String interpreter(final String inputExpression) {
        IExpr result;
        final StringWriter buf = new StringWriter();
        try {
            result = (IExpr) consoleEngine.eval(inputExpression);
            if (result.equals(F.Null)) {
                return "";
            }
            return result.toString();
        } catch (final RuntimeException re) {
            printException(buf, re);
        } catch (final Exception e) {
            printException(buf, e);
        }
        return buf.toString();
    }

    private void printException(final Writer buf, final Throwable e) {
        String msg = e.getMessage();
        try {
            if (msg != null) {
                buf.write("\nError: " + msg);

            } else {
                buf.write("\nError: " + e.getClass().getSimpleName());
            }
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }
    public Main() {
        scriptManager = new ScriptEngineManager();
        consoleEngine = scriptManager.getEngineByExtension("m");
        ScriptContext context = consoleEngine.getContext();
        context.setAttribute(SymjaScriptEngine.RETURN_OBJECT, Boolean.TRUE, ScriptContext.ENGINE_SCOPE);
        context.setAttribute("ENABLE_HISTORY", Boolean.TRUE, ScriptContext.ENGINE_SCOPE);
    }
}
