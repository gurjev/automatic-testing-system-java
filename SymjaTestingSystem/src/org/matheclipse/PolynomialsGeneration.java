package org.matheclipse;

import java.util.Random;

/**
 * Created by Пользователь on 05.05.2016.
 */

public class PolynomialsGeneration {
    String factors="";
    int i=0;
    public String FindFactors(int rootsNumber) {    // функция нахождения множителей
        int [] roots = GenerateRoots(rootsNumber);
        while (i < rootsNumber) {
            factors = factors + "(x-(" + roots[i] + "))*";
            i++;
        }
        factors = factors.substring(0,factors.length()-1);
        return factors;
    }

    public String FindPolynomials(String factors){  // функция нахождения многочлена по его множителям
        Main main = new Main();
        String polynomial = main.interpreter("Expand[" + factors + "]");
        return polynomial;
    }
    public int[] GenerateRoots(int rootsNumber) {   // генерация корней многочлена
        int[] roots = new int[rootsNumber];
        Random rnd = new Random();
        for (int c = 0; c < rootsNumber; ++c) roots[c] = rnd.nextInt(21) - 10;
        return roots;
    }
}
