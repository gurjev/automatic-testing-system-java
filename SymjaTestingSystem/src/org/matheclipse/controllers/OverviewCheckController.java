package org.matheclipse.controllers;

import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.matheclipse.Main;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Пользователь on 26.05.2016.
 */
public class OverviewCheckController {
    @FXML
    private TextField filePath;
    @FXML
    private TableView<Check> table;
    @FXML
    private Label scoreLabel;
    @FXML
    private Label nameLabel;

    File file;

    Main main = new Main();
    String [] correctAnswer = new String[100];
    String [] studentAnswer = new String[100];
    int score = 0;

    List<Check> checks = new ArrayList<Check>();

    public class Check {
        private final StringProperty correctAnswer ;
        private final StringProperty studentAnswer ;
        Check(String correctAnswer, String studentAnswer) {
            this.correctAnswer = new SimpleStringProperty(this, "firstName", correctAnswer);
            this.studentAnswer = new SimpleStringProperty(this, "studentAnswer", studentAnswer);
        }
        public String getCorrectAnswer() { return correctAnswer.get(); }
        public void setCorrectAnswer(String correctAnswer) { this.correctAnswer.set(correctAnswer);}
        public StringProperty correctAnswerProperty() { return correctAnswer ; }
        public String getStudentAnswer() { return studentAnswer.get(); }
        public void setStudentAnswer(String studentAnswer) { this.studentAnswer.set(studentAnswer); }
        public StringProperty studentAnswerProperty() { return studentAnswer ; }
    }

    @FXML
    private void chooseFile(){
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Выберите файл XML");
        file = fileChooser.showOpenDialog(filePath.getScene().getWindow());
        filePath.setText(file.getAbsolutePath());
    }

    @FXML
    public void checkAndReport() throws Exception {
        if (filePath.getLength() != 0) {
            int i = 0;
            File inputFile = new File(filePath.getText());
            DocumentBuilderFactory dbFactory
                    = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(inputFile);
            doc.getDocumentElement().normalize();
            NodeList nList = doc.getElementsByTagName("question");
            int pagesCount = doc.getElementsByTagName("question").getLength();
            String name = doc.getDocumentElement().getAttribute("name");
            while (i < pagesCount) {
                Node nNode = nList.item(i);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    correctAnswer[i] = eElement.getElementsByTagName("correctAnswer").item(0).getTextContent();
                    studentAnswer[i] = eElement.getElementsByTagName("studentAnswer").item(0).getTextContent();
                    if (main.interpreter("Equal[(" + correctAnswer[i] + "), (" + studentAnswer[i] + ")]").equals("True")) {
                        score++;
                    }

                    checks.add(new Check(correctAnswer[i], studentAnswer[i]));
                    i++;
                }
            }
            table.getItems().addAll(checks);
            scoreLabel.setText(score + " из " + pagesCount);
            nameLabel.setText("Имя ученика: " + name);
        }
        else {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Внимание");
            alert.setHeaderText(null);
            alert.setContentText("Выберите файл");
            alert.showAndWait();
        }
    }

    @FXML
    public void openAbout() {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Main.class.getResource("OverviewAbout.fxml"));
        VBox overview = null;
        try {
            overview = (VBox) loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Stage newStage = new Stage();

        Scene scene = new Scene(overview);

        newStage.setScene(scene);
        newStage.getIcons().add(new Image(Main.class.getResourceAsStream("icon.png")));
        newStage.setTitle("О программе");
        newStage.setResizable(false);
        newStage.show();
    }

    @FXML
    private void exitApplication() {
        Platform.exit();
    }

    @FXML
    private void close() {  // закрытие окна
        Stage stage = (Stage) filePath.getScene().getWindow();
        stage.close();
    }
}
