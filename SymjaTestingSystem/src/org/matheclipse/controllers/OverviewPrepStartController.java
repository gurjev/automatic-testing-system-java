package org.matheclipse.controllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import org.matheclipse.Main;

import java.io.IOException;

/**
 * Created by Пользователь on 26.05.2016.
 */
public class OverviewPrepStartController {
    @FXML
    private void openGenerate(){
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Main.class.getResource("OverviewGenerate.fxml"));
        VBox overview = null;
        try {
            overview = (VBox) loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Stage newStage = new Stage();

        Scene scene = new Scene(overview);

        newStage.setScene(scene);
        newStage.getIcons().add(new Image(Main.class.getResourceAsStream("icon.png")));
        newStage.setTitle("Генерация заданий");
        newStage.setResizable(false);
        newStage.show();
    }
    @FXML
    private void openCheck(){
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Main.class.getResource("OverviewCheck.fxml"));
        VBox overview = null;
        try {
            overview = (VBox) loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Stage newStage = new Stage();

        Scene scene = new Scene(overview);

        newStage.setScene(scene);
        newStage.getIcons().add(new Image(Main.class.getResourceAsStream("icon.png")));
        newStage.setTitle("Проверка и отчет");
        newStage.setResizable(false);
        newStage.show();
    }
}
