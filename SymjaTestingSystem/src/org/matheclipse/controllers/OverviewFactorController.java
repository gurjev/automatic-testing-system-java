package org.matheclipse.controllers;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import org.matheclipse.Main;
import org.matheclipse.PolynomialsGeneration;

import java.io.IOException;

/**
 * Created by Пользователь on 23.05.2016.
 */
public class OverviewFactorController {
    int globalCheckVar;
    @FXML
    private ImageView imgCorrect1;
    @FXML
    private ImageView imgCorrect2;
    @FXML
    private ImageView imgCorrect3;
    @FXML
    private ImageView imgCorrect4;
    @FXML
    private ImageView imgCorrect5;
    @FXML
    private ImageView imgWrong1;
    @FXML
    private ImageView imgWrong2;
    @FXML
    private ImageView imgWrong3;
    @FXML
    private ImageView imgWrong4;
    @FXML
    private ImageView imgWrong5;


    @FXML
    private TextField polynomialsArea1;
    @FXML
    private TextField polynomialsArea2;
    @FXML
    private TextField polynomialsArea3;
    @FXML
    private TextField polynomialsArea4;
    @FXML
    private TextField polynomialsArea5;

    @FXML
    private TextField rootsNumber;
    @FXML
    private TextField yourAnswer1;
    @FXML
    private TextField yourAnswer2;
    @FXML
    private TextField yourAnswer3;
    @FXML
    private TextField yourAnswer4;
    @FXML
    private TextField yourAnswer5;

    @FXML
    private TextField isRight;


    @FXML
    private void handleGenerateButton() {  // обработчик нажатия кнопки генерации
        polynomialsArea1.clear();
        polynomialsArea2.clear();
        polynomialsArea3.clear();
        polynomialsArea4.clear();
        polynomialsArea5.clear();
        int input = 0;
        if ((rootsNumber.getText().contains(".")) || (rootsNumber.getText().contains(","))) {
            polynomialsArea1.setText("input must be integer");
        } else {
            input = Integer.parseInt(rootsNumber.getText());
            if (input > 1) {
                setPolynomialToTextField(input, polynomialsArea1);
                setPolynomialToTextField(input, polynomialsArea2);
                setPolynomialToTextField(input, polynomialsArea3);
                setPolynomialToTextField(input, polynomialsArea4);
                setPolynomialToTextField(input, polynomialsArea5);
            } else polynomialsArea1.setText("Degree of a polynomial must be bigger than 1");
        }

    }

    @FXML
    private void handleCheckButton() {  // обработчик нажатия кнопки проверки ответа
        globalCheckVar = 0;
        checkFactors(polynomialsArea1, yourAnswer1, imgCorrect1, imgWrong1);
        checkFactors(polynomialsArea2, yourAnswer2, imgCorrect2, imgWrong2);
        checkFactors(polynomialsArea3, yourAnswer3, imgCorrect3, imgWrong3);
        checkFactors(polynomialsArea4, yourAnswer4, imgCorrect4, imgWrong4);
        checkFactors(polynomialsArea5, yourAnswer5, imgCorrect5, imgWrong5);
        isRight.setText(Integer.toString(globalCheckVar));
    }

    public void setPolynomialToTextField(int input, TextField text) {
        PolynomialsGeneration pg = new PolynomialsGeneration();
        String factors = pg.FindFactors(input);
        text.setText(pg.FindPolynomials(factors));
    }

    public void checkFactors(TextField polynomial, TextField answer, ImageView correct, ImageView wrong) {
        Main main = new Main();
        String correctAnswer = main.interpreter("Factor[" + polynomial.getText() + "]");
        String equal = main.interpreter("Equal[(" + answer.getText() + "),(" + correctAnswer + ")]");
        if (equal.equals("True")) {
            globalCheckVar = globalCheckVar + 1;
            correct.setVisible(true);
            wrong.setVisible(false);
        } else {
            wrong.setVisible(true);
            correct.setVisible(false);
        }
    }

    @FXML
    private void handleEnterPressed(KeyEvent event) {
        if (event.getCode() == KeyCode.ENTER) {
            handleGenerateButton();
        }
    }

    @FXML
    private void handleF1Pressed(KeyEvent event) {
        if (event.getCode() == KeyCode.F1) {
            openAbout();
        }
    }

    @FXML
    private void close() {  // закрытие окна
        Stage stage = (Stage) rootsNumber.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void exitApplication() {
        Platform.exit();
    }

    @FXML
    public void openAbout() {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Main.class.getResource("OverviewAbout.fxml"));
        VBox overview = null;
        try {
            overview = (VBox) loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Stage newStage = new Stage();

        Scene scene = new Scene(overview);

        newStage.setScene(scene);
        newStage.getIcons().add(new Image(Main.class.getResourceAsStream("icon.png")));
        newStage.setTitle("О программе");
        newStage.setResizable(false);
        newStage.show();
    }

}
