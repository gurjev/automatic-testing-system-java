package org.matheclipse.controllers;

import com.sun.xml.internal.bind.v2.runtime.reflect.opt.Const;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import org.matheclipse.Abstract.ABC;
import org.matheclipse.Main;
import org.matheclipse.classes.FactorFreeValue;
import org.matheclipse.classes.LinearEquation;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Пользователь on 23.05.2016.
 */
public class OverviewGenerateController {
    public class Pair{
        String representation;
        Constructor constructor;
        public Pair(String representation, Constructor constructor)
        {
            this.representation = representation;
            this.constructor = constructor;
        }
        public Constructor getConstructor() {
            return constructor;
        }
        public String toString() {
            return this.representation;
        }
    }

    ArrayList<Pair> questionList = new ArrayList<>();
    List<Constructor> constructorList;
    int i = 0;

    @FXML
    private Button addQuestionButton;
    @FXML
    private Button removeQuestionButton;
    @FXML
    private TextField xmlNumber;
    @FXML
    private TextArea questionArea;
    @FXML
    private ChoiceBox<Pair> choiceBox;
    @FXML
    private ComboBox<Pair> comboBox;
    @FXML
    private void initialize() {
        //chooseClass.getItems()
        try {
            choiceBox.getItems().add(new Pair("Линейные уравнения", LinearEquation.class.getDeclaredConstructor()));
            choiceBox.getItems().add(new Pair("Разложить на множители", FactorFreeValue.class.getDeclaredConstructor()));
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        //chooseClass.getItems().addAll("Линейные уравнения",
                                    //  "Разложение на множители");
    }
    @FXML
    private void addQuestion() {
        if (choiceBox.getSelectionModel().getSelectedItem() == null) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Внимание");
            alert.setHeaderText(null);
            alert.setContentText("Выберите тип вопроса");

            alert.showAndWait();
        }
        else{
            questionList.add(choiceBox.getSelectionModel().getSelectedItem());
            questionArea.appendText(questionList.get(i).toString() + "\n");
            i++;
        }
    }

    //ABC question = (ABC)choiceBox.getItems().get(0).getConstructor().newInstance();
    //question.generate();

    @FXML
    private void removeQuestion(){
        if (questionList.size() != 0) {
            questionList.remove(questionList.size() - 1);
            questionArea.clear();
            for (int j = 0; j < i - 1; j++) {
                questionArea.appendText(questionList.get(j).toString() + "\n");
            }
            i--;
        }
    }

    @FXML
    private void generateXMLs(){
        for (int k = 1; k <= Integer.parseInt(xmlNumber.getText()); k++) {
            try {
                DocumentBuilderFactory dbFactory =
                        DocumentBuilderFactory.newInstance();
                DocumentBuilder dBuilder =
                        dbFactory.newDocumentBuilder();
                Document doc = dBuilder.newDocument();
                Element rootElement = doc.createElement("pages");
                doc.appendChild(rootElement);
                Attr attr1 = doc.createAttribute("name");
                attr1.setValue("");
                rootElement.setAttributeNode(attr1);
                Attr attr2 = doc.createAttribute("pagesCount");
                attr2.setValue("0");
                rootElement.setAttributeNode(attr2);

                for (int t = 0; t <= questionList.size() - 1; t++) {
                    try {
                        ABC question = (ABC)questionList.get(t).getConstructor().newInstance();
                        question.generate();
                        question.serialize(doc);
                    } catch (InstantiationException e) {
                        e.printStackTrace();
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    } catch (InvocationTargetException e) {
                        e.printStackTrace();
                    }
                }


                TransformerFactory transformerFactory =
                        TransformerFactory.newInstance();
                Transformer transformer =
                        transformerFactory.newTransformer();
                DOMSource source = new DOMSource(doc);
                StreamResult result =
                        new StreamResult(new File("C:\\Users\\Пользователь\\Desktop\\ATSJ\\SymjaTestingSystem\\src\\org\\matheclipse\\test" + k + ".xml"));
                transformer.transform(source, result);


            } catch (ParserConfigurationException e) {
                e.printStackTrace();
            } catch (TransformerConfigurationException e) {
                e.printStackTrace();
            } catch (TransformerException e) {
                e.printStackTrace();
            }
        }
    }

    @FXML
    public void openAbout() {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Main.class.getResource("OverviewAbout.fxml"));
        VBox overview = null;
        try {
            overview = (VBox) loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Stage newStage = new Stage();

        Scene scene = new Scene(overview);

        newStage.setScene(scene);
        newStage.getIcons().add(new Image(Main.class.getResourceAsStream("icon.png")));
        newStage.setTitle("О программе");
        newStage.setResizable(false);
        newStage.show();
    }

    @FXML
    private void exitApplication() {
        Platform.exit();
    }

    @FXML
    private void close() {  // закрытие окна
        Stage stage = (Stage) addQuestionButton.getScene().getWindow();
        stage.close();
    }
}
