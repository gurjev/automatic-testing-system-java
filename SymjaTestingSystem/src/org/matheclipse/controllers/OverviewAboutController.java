package org.matheclipse.controllers;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import org.matheclipse.Main;

/**
 * Created by Пользователь on 23.05.2016.
 */
public class OverviewAboutController {
    @FXML
    private Button backButtonFactor;
    @FXML
    private void goToBitbucket() {
        Main main = new Main();
        main.getHostServices().showDocument("https://bitbucket.org/gurjev/automatic-testing-system-java");
    }

    @FXML
    private void exitApplication() {
        Platform.exit();
    }
    @FXML
    private void close() {  // закрытие окна
        Stage stage = (Stage) backButtonFactor.getScene().getWindow();
        stage.close();
    }
}
