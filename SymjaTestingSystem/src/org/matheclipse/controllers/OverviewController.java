package org.matheclipse.controllers;

/**
 * Created by Пользователь on 05.05.2016.
 */
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.matheclipse.Main;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import org.w3c.dom.*;

public class OverviewController {
    @FXML
    private TextField nameField;
    @FXML
    private RadioButton radioButton1;
    @FXML
    private RadioButton radioButton2;
    @FXML
    private RadioButton radioButton3;
    @FXML
    private RadioButton radioButton4;
    @FXML
    private RadioButton radioButton5;
    @FXML
    private RadioButton radioButton6;
    @FXML
    private CheckBox checkBox1;
    @FXML
    private CheckBox checkBox2;
    @FXML
    private CheckBox checkBox3;
    @FXML
    private CheckBox checkBox4;
    @FXML
    private CheckBox checkBox5;
    @FXML
    private CheckBox checkBox6;

    String testName = "test1.xml";

    public String answerSingleChoice = null;
    public String answerMultipleChoice = null;
    public String answerFreeValue = null;

    private ArrayList<String> rolev;
    public int pagesCount = 0;
    public int currentPage = 0;
    public Stage newStage = new Stage(); //
    public List<Pane> panesList = new ArrayList<Pane>();

    public int pageNumber = 0;

    public int timeMinutes = 45;
    //public int timeSeconds = 360;


    public OverviewController() {  // Конструктор класса

    }

    @FXML
    private void initialize() {

    }

    Main main2; // Ссылка на главный класс приложения

    public void setMain(Main main) {
        this.main2 = main;
    }

    @FXML
    private void openFactorization() {  //открытие окна проверки разложения на множители
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Main.class.getResource("OverviewFactor.fxml"));
        VBox overview = null;
        try {
            overview = (VBox) loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Stage newStage = new Stage();

        Scene scene = new Scene(overview);

        newStage.setScene(scene);
        newStage.getIcons().add(new Image(Main.class.getResourceAsStream("icon.png")));
        newStage.setTitle("Разложение на множители");
        newStage.setResizable(false);
        newStage.show();
    }

    @FXML
    private void openExpansion() {  // открытие окна проверки раскрытия скобок
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Main.class.getResource("OverviewExpand.fxml"));
        VBox overview = null;
        try {
            overview = (VBox) loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Stage newStage = new Stage();

        Scene scene = new Scene(overview);

        newStage.setScene(scene);
        newStage.getIcons().add(new Image(Main.class.getResourceAsStream("icon.png")));
        newStage.setTitle("Раскрытие скобок");
        newStage.setResizable(false);
        newStage.show();
    }

    @FXML
    public void openAbout() {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Main.class.getResource("OverviewAbout.fxml"));
        VBox overview = null;
        try {
            overview = (VBox) loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Stage newStage = new Stage();

        Scene scene = new Scene(overview);

        newStage.setScene(scene);
        newStage.getIcons().add(new Image(Main.class.getResourceAsStream("icon.png")));
        newStage.setTitle("О программе");
        newStage.setResizable(false);
        newStage.show();
    }

    @FXML
    private void exitApplication() {
        Platform.exit();
    }

    @FXML
    private void openGenerate(){  // Открытие окна настройки теста
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Main.class.getResource("OverviewPrepStart.fxml"));
        VBox overview = null;
        try {
            overview = (VBox) loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Scene scene = new Scene(overview);
        //chooseClass.getItems().addAll(l, f);

        newStage.setScene(scene);
        newStage.getIcons().add(new Image(Main.class.getResourceAsStream("icon.png")));
        newStage.setTitle("Преподавательская часть");
        newStage.setResizable(false);
        newStage.show();
    }

    @FXML
    private void openTest() {  // Открытие окна теста (генерация элементов управления на лету)
        if (nameField.getText().equals("")) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Ошибка");
            alert.setHeaderText(null);
            alert.setContentText("Введите ваше ФИО!");
            alert.showAndWait();
        }
        else {
            try {
                File inputFile = new File("C:\\Users\\Пользователь\\Desktop\\ATSJ\\SymjaTestingSystem\\src\\org\\matheclipse\\" + testName);
                DocumentBuilderFactory dbFactory
                        = DocumentBuilderFactory.newInstance();
                DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                Document doc = dBuilder.parse(inputFile);
                doc.getDocumentElement().normalize();

                Attr attr = doc.createAttribute("name");
                attr.setValue(nameField.getText());
                doc.getDocumentElement().setAttributeNode(attr);

                TransformerFactory transformerFactory =
                        TransformerFactory.newInstance();
                Transformer transformer =
                        transformerFactory.newTransformer();
                DOMSource source = new DOMSource(doc);
                StreamResult result =
                        new StreamResult(new File("C:\\Users\\Пользователь\\Desktop\\ATSJ\\SymjaTestingSystem\\src\\org\\matheclipse\\" + testName));
                transformer.transform(source, result);
            }
            catch (Exception w) {
                w.printStackTrace();
            }
            final Label clock = new Label();
            clock.setText("Минут осталось: " + Integer.toString(timeMinutes));
            final Timeline timeline = new Timeline(new KeyFrame(Duration.minutes(1), new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    timeMinutes--;
                    clock.setText("Минут осталось: " + Integer.toString(timeMinutes));
                }
            }));
            timeline.setCycleCount(Animation.INDEFINITE);
            timeline.play();
            final Menu menu = new Menu("Меню");
            final Menu help = new Menu("Помощь");
            MenuBar menuBar = new MenuBar();
            menuBar.prefWidthProperty().bind(newStage.widthProperty());
            //menuBar.setPrefWidth(700);
            MenuItem getBack = new MenuItem("Отменить тестирование");
            getBack.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    Stage stage = (Stage) clock.getScene().getWindow();
                    stage.close();
                }
            });
            MenuItem exitApp = new MenuItem("Выйти");
            exitApp.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    exitApplication();
                }
            });
            menu.getItems().addAll(getBack, exitApp);
            MenuItem about = new MenuItem("О программе");
            about.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    openAbout();
                }
            });
            MenuItem blitzFactor = new MenuItem("Блиц-тест \"Разложение на множители\"");
            blitzFactor.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    openFactorization();
                }
            });
            MenuItem blitzExpand = new MenuItem("Блиц-тест \"Раскрытие скобок\"");
            blitzExpand.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    openExpansion();
                }
            });
            help.getItems().addAll(blitzFactor, blitzExpand, about);

            menuBar.getMenus().addAll(menu, help);

            clock.relocate(200, 480);
            clock.setFont(Font.font("System", 30));
            //некоторый код
            readXML();

            double y = (1) / (double) (pagesCount);
            ProgressBar progressBar = new ProgressBar(y);
            progressBar.prefWidthProperty().bind(newStage.widthProperty());
            progressBar.relocate(0, 440);

            //AnchorPane anchorPane = new AnchorPane();

            Pane pane = new Pane();

            int i = 0;
            Label pageShower = new Label();
            pageShower.relocate(300, 30);
            pageShower.setFont(Font.font("Arial", 20));
            pageShower.setText("ЗАДАНИЕ " + Integer.toString(pageNumber + 1) + "/" + Integer.toString(pagesCount));
            Button next = new Button();
            next.relocate(640, 30);
            Button back = new Button();
            back.relocate(0, 30);
            back.setText("Назад");
            next.setText("Вперед");
            while (i < pagesCount) {
                pane.getChildren().addAll(panesList.get(i));
                i++;
            }
            pane.getChildren().addAll(next, back, clock, pageShower, progressBar, menuBar);
            back.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent e) {
                    if ((pageNumber > 0) && (pagesCount > pageNumber)) {
                        next.relocate(640, 30);
                        next.setText("Вперед");
                        pane.getChildren().get(pageNumber).setVisible(false);
                        pane.getChildren().get(pageNumber - 1).setVisible(true);
                        pageNumber--;
                    } else {
                        Alert alert = new Alert(Alert.AlertType.INFORMATION);
                        alert.setTitle("Information Dialog");
                        alert.setHeaderText(null);
                        alert.setContentText("Вы на первой странице");

                        alert.showAndWait();
                    }

                    pageShower.setText("ЗАДАНИЕ " + Integer.toString(pageNumber + 1) + "/" + Integer.toString(pagesCount));
                    double x = (pageNumber + 1) / (double) (pagesCount);
                    progressBar.setProgress((pageNumber + 1) / (double) (pagesCount));
                    System.out.println(x);
                }
            });
            next.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent e) {
                    if (pageNumber == pagesCount-2){
                        next.setText("Сохранить и закончить тестирование");
                        next.relocate(480, 30);
                    }
                    if ((pageNumber >= 0) && (pagesCount - 1 > pageNumber)) {
                        pane.getChildren().get(pageNumber).setVisible(false);
                        pane.getChildren().get(pageNumber + 1).setVisible(true);
                        pageNumber++;
                    } else {
                        Alert alert = new Alert(Alert.AlertType.INFORMATION);
                        alert.setTitle("Поздравляем");
                        alert.setHeaderText(null);
                        alert.setContentText("Спасибо, что прошли тест!");
                        alert.showAndWait();
                        Stage stage = (Stage) clock.getScene().getWindow();
                        stage.close();
                    }

                    pageShower.setText("ЗАДАНИЕ " + Integer.toString(pageNumber + 1) + "/" + Integer.toString(pagesCount));
                    progressBar.setProgress((pageNumber + 1) / (double) (pagesCount));
                    System.out.println((pageNumber + 1) / (double) (pagesCount));
                }
            });
            //anchorPane.getChildren().addAll(pane);
            pane.getChildren().get(pageNumber).setVisible(true);
            Scene scene = new Scene(pane);

            newStage.setScene(scene);
            newStage.getIcons().add(new Image(Main.class.getResourceAsStream("icon.png")));
            newStage.setTitle("Тестирование");
            newStage.setResizable(false);
            newStage.show();
        }
    }

    public void readXML() {  // Чтение XML с индивидуальным заданием ученика
        String[] vars = new String[10];
        //List<String> var = new ArrayList<String>();
        try {
            File inputFile = new File("C:\\Users\\Пользователь\\Desktop\\ATSJ\\SymjaTestingSystem\\src\\org\\matheclipse\\" + testName);
            DocumentBuilderFactory dbFactory
                    = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(inputFile);
            doc.getDocumentElement().normalize();
            NodeList nList = doc.getElementsByTagName("question");
            pagesCount = doc.getElementsByTagName("question").getLength();
            while (currentPage < pagesCount) {
                Node nNode = nList.item(currentPage);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    String type = eElement.getAttribute("type");
                    System.out.println(type);
                    String text = eElement.getElementsByTagName("text").item(0).getTextContent();
                    int varsCount = eElement.getElementsByTagName("var").getLength();
                    System.out.println(varsCount);
                    for (int i = 0; i < varsCount; i++) {
                        vars[i] = eElement.getElementsByTagName("var").item(i).getTextContent();
                    }
                    addPage(text, type, varsCount, vars);
                    currentPage++;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Pane addPage(String text, String type, int varsCount, String[] vars){
        int i = 0;
        Pane pane = new Pane();
        pane.setPrefSize(640, 480);
        List <RadioButton> radiobuttons = new ArrayList<RadioButton>();
        List <CheckBox> checkboxes = new ArrayList<CheckBox>();
        radiobuttons.add(radioButton1 = new RadioButton());
        radiobuttons.add(radioButton2 = new RadioButton());
        radiobuttons.add(radioButton3 = new RadioButton());
        radiobuttons.add(radioButton4 = new RadioButton());
        radiobuttons.add(radioButton5 = new RadioButton());
        radiobuttons.add(radioButton6 = new RadioButton());
        checkboxes.add(checkBox1 = new CheckBox());
        checkboxes.add(checkBox2 = new CheckBox());
        checkboxes.add(checkBox3 = new CheckBox());
        checkboxes.add(checkBox4 = new CheckBox());
        checkboxes.add(checkBox5 = new CheckBox());
        checkboxes.add(checkBox6 = new CheckBox());
        ToggleGroup group = new ToggleGroup();
        //unVisibleAll();
        if (type.equals("singlechoice")){
            Label done = new Label();
            done.setText("Ответ записан!");
            done.relocate(300, 230);
            done.setVisible(false);
            GridPane grid = new GridPane();
            grid.setHgap(5);
            grid.setVgap(5);
            grid.relocate(100, 130);
            WebView html = new WebView();
            html.setPrefSize(500, 70);
            WebEngine webEngine = html.getEngine();
            webEngine.loadContent(text);
            html.relocate(100, 70);
            Button button = new Button();
            button.setText("Ответить");
            button.relocate(300, 200);
            while (i < varsCount){
                radiobuttons.get(i).setToggleGroup(group);
                grid.add(radiobuttons.get(i), 0, i);
                radiobuttons.get(i).setText(vars[i]); i++;
            }
            group.selectedToggleProperty().addListener(new ChangeListener<Toggle>(){
                public void changed(ObservableValue<? extends Toggle> ov,
                                    Toggle old_toggle, Toggle new_toggle) {
                    if (group.getSelectedToggle() != null) {
                        answerSingleChoice = group.getSelectedToggle().getUserData().toString();
                    }
                }
            });
            button.setOnAction(new EventHandler<ActionEvent>() {
                        @Override public void handle(ActionEvent e) {
                            try {
                                File inputFile = new File("C:\\Users\\Пользователь\\Desktop\\ATSJ\\SymjaTestingSystem\\src\\org\\matheclipse\\" + testName);
                                DocumentBuilderFactory dbFactory
                                        = DocumentBuilderFactory.newInstance();
                                DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                                Document doc = dBuilder.parse(inputFile);
                                doc.getDocumentElement().normalize();

                                writeToXML(doc, answerSingleChoice, pageNumber);
                                done.setVisible(true);
                            }
                            catch (Exception w) {
                                w.printStackTrace();
                            }
                        }
            });
            pane.getChildren().addAll(grid, button, html, done);
            pane.setVisible(false);
            panesList.add(pane);
        }
        if (type.equals("multiplechoice")){
            Label done = new Label();
            done.setText("Ответ записан!");
            done.relocate(300, 230);
            done.setVisible(false);
            GridPane grid = new GridPane();
            grid.setHgap(5);
            grid.setVgap(5);
            grid.relocate(100, 130);
            WebView html = new WebView();
            html.setPrefSize(500, 70);
            WebEngine webEngine = html.getEngine();
            webEngine.loadContent(text);
            html.relocate(100, 70);
            Button button = new Button();
            button.setText("Ответить");
            button.relocate(300, 200);
            while (i < varsCount){
                grid.add(checkboxes.get(i), 0, i);
                checkboxes.get(i).setText(vars[i]);
                i++;
                //radiobuttons.get(i).setVisible(true);
            }
            button.setOnAction(new EventHandler<ActionEvent>() {
                @Override public void handle(ActionEvent e) {
                    try {
                        File inputFile = new File("C:\\Users\\Пользователь\\Desktop\\ATSJ\\SymjaTestingSystem\\src\\org\\matheclipse\\" + testName);
                        DocumentBuilderFactory dbFactory
                                = DocumentBuilderFactory.newInstance();
                        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                        Document doc = dBuilder.parse(inputFile);
                        doc.getDocumentElement().normalize();
                        for (int i = 0; i < 6; i++){
                            if (checkboxes.get(i).isSelected()){
                                answerMultipleChoice = answerMultipleChoice + checkboxes.get(i).getText() + ", ";
                            }
                        }
                        answerMultipleChoice = answerMultipleChoice.substring(0,answerMultipleChoice.length()-2);
                        writeToXML(doc, answerMultipleChoice, pageNumber);

                        done.setVisible(true);
                    }
                    catch (Exception w) {
                        w.printStackTrace();
                    }
                }
            });
            pane.getChildren().addAll(grid, button, html, done);
            pane.setVisible(false);
            panesList.add(pane);
        }
        if (type.equals("freevalue")){
            Label done = new Label();
            done.setText("Ответ записан!");
            done.relocate(300, 230);
            done.setVisible(false);
            WebView html = new WebView();
            html.setPrefSize(500, 70);
            WebEngine webEngine = html.getEngine();
            webEngine.loadContent(text);
            html.relocate(100, 70);
            TextField freeValue = new TextField();
            freeValue.relocate(100, 200);
            freeValue.setPrefWidth(350);
            Button button = new Button();
            button.setText("Ответить");
            button.relocate(500, 200);
            button.setOnAction(new EventHandler<ActionEvent>() {
                @Override public void handle(ActionEvent e) {
                    try {
                        File inputFile = new File("C:\\Users\\Пользователь\\Desktop\\ATSJ\\SymjaTestingSystem\\src\\org\\matheclipse\\" + testName);
                        DocumentBuilderFactory dbFactory
                                = DocumentBuilderFactory.newInstance();
                        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                        Document doc = dBuilder.parse(inputFile);
                        doc.getDocumentElement().normalize();

                        writeToXML(doc, freeValue.getText(), pageNumber);
                        done.setVisible(true);
                    }
                    catch (Exception w) {
                        w.printStackTrace();
                    }
                }
            });
            pane.getChildren().addAll(freeValue, button, html, done);
            pane.setVisible(false);
            panesList.add(pane);
        }
        return pane;
    }

    public void writeToXML(Document document, String answer, int pageNumber){
            try {
                document.getElementsByTagName("studentAnswer").item(pageNumber).setTextContent(answer);
                TransformerFactory transformerFactory =
                        TransformerFactory.newInstance();
                Transformer transformer =
                        transformerFactory.newTransformer();
                DOMSource source = new DOMSource(document);
                StreamResult result =
                        new StreamResult(new File("C:\\Users\\Пользователь\\Desktop\\ATSJ\\SymjaTestingSystem\\src\\org\\matheclipse\\" + testName));
                transformer.transform(source, result);
            } catch (TransformerException e) {
                e.printStackTrace();
            }
    }

    @FXML
    private void handleF1Pressed(KeyEvent event) {
        if (event.getCode() == KeyCode.F1) {
            openAbout();
        }
    }
}
