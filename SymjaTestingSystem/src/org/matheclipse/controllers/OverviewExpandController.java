package org.matheclipse.controllers;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import org.matheclipse.Main;
import org.matheclipse.PolynomialsGeneration;

import java.io.IOException;

/**
 * Created by Пользователь on 23.05.2016.
 */
public class OverviewExpandController {
    int globalCheckVar;
    @FXML
    private ImageView imgCorrectExpand1;
    @FXML
    private ImageView imgCorrectExpand2;
    @FXML
    private ImageView imgCorrectExpand3;
    @FXML
    private ImageView imgCorrectExpand4;
    @FXML
    private ImageView imgCorrectExpand5;
    @FXML
    private ImageView imgWrongExpand1;
    @FXML
    private ImageView imgWrongExpand2;
    @FXML
    private ImageView imgWrongExpand3;
    @FXML
    private ImageView imgWrongExpand4;
    @FXML
    private ImageView imgWrongExpand5;


    @FXML
    private TextField factorsNumber;
    @FXML
    private Button backButtonFactor;
    @FXML
    private TextField factorsArea1;
    @FXML
    private TextField factorsArea2;
    @FXML
    private TextField factorsArea3;
    @FXML
    private TextField factorsArea4;
    @FXML
    private TextField factorsArea5;
    @FXML
    private TextField yourAnswerExpand1;
    @FXML
    private TextField yourAnswerExpand2;
    @FXML
    private TextField yourAnswerExpand3;
    @FXML
    private TextField yourAnswerExpand4;
    @FXML
    private TextField yourAnswerExpand5;
    @FXML
    private TextField isRightExpand;




    @FXML
    private void close() {  // закрытие окна
        Stage stage = (Stage) backButtonFactor.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void handleEnterPressedExpand(KeyEvent event) {
        if (event.getCode() == KeyCode.ENTER) {
            handleGenerateFactorsButton();
        }

    }

    @FXML
    private void handleCheckExpandButton() {     // проверка правильности ответа
        globalCheckVar = 0;
        checkPolynomials(factorsArea1, yourAnswerExpand1, imgCorrectExpand1, imgWrongExpand1);
        checkPolynomials(factorsArea2, yourAnswerExpand2, imgCorrectExpand2, imgWrongExpand2);
        checkPolynomials(factorsArea3, yourAnswerExpand3, imgCorrectExpand3, imgWrongExpand3);
        checkPolynomials(factorsArea4, yourAnswerExpand4, imgCorrectExpand4, imgWrongExpand4);
        checkPolynomials(factorsArea5, yourAnswerExpand5, imgCorrectExpand5, imgWrongExpand5);
        isRightExpand.setText(Integer.toString(globalCheckVar));
    }

    @FXML
    private void handleGenerateFactorsButton() {     // генерация множителей
        factorsArea1.clear();
        factorsArea2.clear();
        factorsArea3.clear();
        factorsArea4.clear();
        factorsArea5.clear();

        int input = 0;
        if ((factorsNumber.getText().contains(".")) || (factorsNumber.getText().contains(","))) {
            factorsArea1.setText("input must be integer");
        } else {
            input = Integer.parseInt(factorsNumber.getText());
            if (input > 1) {
                setFactorsToTextField(input, factorsArea1);
                setFactorsToTextField(input, factorsArea2);
                setFactorsToTextField(input, factorsArea3);
                setFactorsToTextField(input, factorsArea4);
                setFactorsToTextField(input, factorsArea5);
            } else factorsArea1.appendText("Number of factors must be bigger than 1");
        }
    }

    public void checkPolynomials(TextField factors, TextField answer, ImageView correct, ImageView wrong) {
        Main main = new Main();
        String correctAnswer = main.interpreter("Expand[" + factors.getText() + "]");
        String equal = main.interpreter("Equal[(" + answer.getText() + "),(" + correctAnswer + ")]");
        if (equal.equals("True")) {
            globalCheckVar = globalCheckVar + 1;
            correct.setVisible(true);
            wrong.setVisible(false);
        } else {
            wrong.setVisible(true);
            correct.setVisible(false);
        }
    }

    public void setFactorsToTextField(int input, TextField text) {
        PolynomialsGeneration pg = new PolynomialsGeneration();
        String factors = pg.FindFactors(input);
        text.setText(factors + "\n");
    }

    @FXML
    public void handleF1Pressed(KeyEvent event) {
        if (event.getCode() == KeyCode.F1) {
            OverviewController o = new OverviewController();
            openAbout();
        }
    }

    @FXML
    private void exitApplication() {
        Platform.exit();
    }

    @FXML
    public void openAbout() {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Main.class.getResource("OverviewAbout.fxml"));
        VBox overview = null;
        try {
            overview = (VBox) loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Stage newStage = new Stage();

        Scene scene = new Scene(overview);

        newStage.setScene(scene);
        newStage.getIcons().add(new Image(Main.class.getResourceAsStream("icon.png")));
        newStage.setTitle("О программе");
        newStage.setResizable(false);
        newStage.show();
    }
}
