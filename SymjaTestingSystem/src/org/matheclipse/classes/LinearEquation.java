package org.matheclipse.classes;

import org.matheclipse.Abstract.FreeValue;
import org.matheclipse.Main;

import java.util.Random;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

/*
Генерация линейного уравнения вида a*x+b=0
 */
public class LinearEquation extends FreeValue {
    int x = 0;
    String text = null;

    // Ссылка на главный класс приложения, чтобы потом пользоваться интерпретатором SymJa
    //Main mainmain;

    // Конструктор, нужен чтобы сохранить ссылку на экземпляр главного класса приложения
    /*public LinearEquation(Main main) {
        this.mainmain = main;
    }*/

    // Генерация линейного уравнения, сохраняется текст задания и решение уравнения
    @Override
    public void generate() {
        Main main = new Main();
        Random random = new Random();
        x = random.nextInt(100) - 50;
        int a = random.nextInt(100) - 50;
        text = "<p>Найдите решение линейного уравнения:";
        text = text + "<img src=\"http://latex.codecogs.com/svg.latex?" + main.interpreter("Expand[(" + Integer.toString(a) + ")*(x-(" + Integer.toString(x) + "))]");
        text = text + "=0\"" + "/></p>";
    }

    // Запись задания в XML-файл
    @Override
    public void serialize(Document document) {
        int pages = Integer.parseInt(document.getDocumentElement().getAttribute("pagesCount"));
        document.getDocumentElement().removeAttribute("pagesCount");
        document.getDocumentElement().setAttribute("pagesCount", Integer.toString(pages + 1));

        Element question = document.createElement("question");
        Attr attrType = document.createAttribute("type");
        attrType.setValue("freevalue");
        question.setAttributeNode(attrType);
        document.getDocumentElement().appendChild(question);

        Element textNode = document.createElement("text");
        textNode.appendChild(document.createTextNode(text));
        question.appendChild(textNode);

        Element answerNode = document.createElement("correctAnswer");
        answerNode.appendChild(document.createTextNode(Integer.toString(x)));
        question.appendChild(answerNode);

        Element studentAnswer = document.createElement("studentAnswer");
        question.appendChild(studentAnswer);
    }

    // Чтение задания из XML-файла
    @Override
    public void deserialize(Element element) {
        Node subElement = element.getElementsByTagName("answer").item(0);
        x = Integer.parseInt(subElement.getTextContent());
        subElement = element.getElementsByTagName("text").item(0);
        text = subElement.getTextContent();
    }

    @Override
    public void check() {

    }

}
