package org.matheclipse.classes;

import org.matheclipse.Abstract.FreeValue;
import org.matheclipse.PolynomialsGeneration;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * Created by Пользователь on 14.05.2016.
 */
//генерация многочленов 2 степени
public class FactorFreeValue extends FreeValue {
    public String text = "Разложите на множители многочлен: ";
    public String answer = null;
    int rootsNumber = 2;

    @Override
    public void generate() {
        PolynomialsGeneration pg = new PolynomialsGeneration();
        String factors = pg.FindFactors(rootsNumber);
        String polynomial = pg.FindPolynomials(factors);
        text = text + "<img src=\"http://latex.codecogs.com/svg.latex?" + polynomial + "\"/></p>";
        answer = factors;
    }

    @Override
    public void serialize(Document document) {
        int pages = Integer.parseInt(document.getDocumentElement().getAttribute("pagesCount"));
        document.getDocumentElement().removeAttribute("pagesCount");
        document.getDocumentElement().setAttribute("pagesCount", Integer.toString(pages + 1));

        Element question = document.createElement("question");
        Attr attrType = document.createAttribute("type");
        attrType.setValue("freevalue");
        question.setAttributeNode(attrType);
        document.getDocumentElement().appendChild(question);

        Element textNode = document.createElement("text");
        textNode.appendChild(document.createTextNode(text));
        question.appendChild(textNode);

        Element answerNode = document.createElement("correctAnswer");
        answerNode.appendChild(document.createTextNode(answer));
        question.appendChild(answerNode);

        Element studentAnswer = document.createElement("studentAnswer");
        question.appendChild(studentAnswer);
    }

    @Override
    public void deserialize(Element element) {

    }


    @Override
    public void check() {

    }
}
