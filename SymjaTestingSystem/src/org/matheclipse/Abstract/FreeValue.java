package org.matheclipse.Abstract;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * Created by Пользователь on 14.05.2016.
 */
public abstract class FreeValue extends ABC {
    public abstract void generate();
    public abstract void serialize(Document document);
    public abstract void deserialize(Element element);
    public abstract void check();
}
